import React from 'react';
import './Preloader.css';

const Preloader = () => {
  return (
    <div className="lds-facebook">
      <div/>
      <div/>
      <div/>
    </div>
  )
};

export default Preloader;