import React, { Component } from 'react';
import {connect} from "react-redux";
import './App.css';
import {getCityWeather, getCurrentGeo} from "./store/actions/weather";
import Preloader from "./components/UI/Preloader/Preloader";

class App extends Component {
  state = {
    city: ''
  };

  componentDidMount() {
    this.props.getCurrentGeoHandler();
  }

  submitWeatherForm = event => {
    event.preventDefault();
    this.props.getWeatherHandler(this.state.city);
  };

  changeCityHandler = event => {
    this.setState({
      city: event.target.value
    });
  };

  render() {
    const {weather} = this.props;

    return (
      <div className="App">
        <header className="App-header">
          {weather.title} App
        </header>
        <section className="App-current-geo">
          {
            weather.isFetchingGeo ?
              <p>Requesting your geo location...</p>
              :
              <p>Your current geo location: {weather.currentGeo || `undefined (${weather.error.message})`}</p>
          }
        </section>

        <p>Enter city name to get weather</p>
        <form onSubmit={event => this.submitWeatherForm(event)}>
          <div className="App-weather-control">
            <input className="App-weather-input" onChange={this.changeCityHandler} type="text"/>
            <button className="App-weather-button">SEND</button>
          </div>
        </form>

        {
          weather.isFetching ?
            <Preloader/>
            :
            <div>
              {
                weather.data.name ?
                  <React.Fragment>
                    <h3>City data:</h3>
                    <p>Name: {weather.data.name || 'Unknown city'}</p>
                    <p>Temperature: {Math.ceil(weather.data.temperature) || 0}</p>
                  </React.Fragment>
                  :
                  <p>
                    {
                      weather.data.error ?
                        'Sorry, we can\'t find city by this name.'
                        :
                        'No data yet'
                    }
                  </p>
              }
            </div>
        }
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    weather: store.weather
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getWeatherHandler: city => dispatch(getCityWeather(city)),
    getCurrentGeoHandler: () => dispatch(getCurrentGeo())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
