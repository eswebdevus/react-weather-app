import {applyMiddleware, createStore, compose } from "redux";
import {rootReducer} from "./reducers";
import logger from "redux-logger";
import thunk from "redux-thunk";

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk, logger)
);

export const store = createStore(rootReducer, enhancer);

