import axios from "axios";
import {weatherAPIKey} from "../../index";
import {
  FETCH_WEATHER_REQUEST, FETCH_WEATHER_SUCCESS, FETCH_WEATHER_FAILURE,
  FETCH_CURRENT_GEO_REQUEST, FETCH_CURRENT_GEO_SUCCESS, FETCH_CURRENT_GEO_FAILURE
} from "../constants/weather";

axios.defaults.timeout = 180000;

export function getCityWeather(city) {

  return async dispatch => {
    let url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${weatherAPIKey}&units=metric`;

    dispatch(fetchWeatherRequest());

    try {
      const response = await axios.get(url);
      const data = {
        name: response.data.city.name,
        temperature: response.data.list[0].main.temp
      };

      dispatch(fetchWeatherSuccess(data));
    } catch (e) {
      dispatch(fetchWeatherFailure(e));
    }
  }
}

export function getCurrentGeo() {
  return async dispatch => {
    dispatch(fetchCurrentGeoRequest());

    const position = new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject);
    });

    try {
      const test = await position;
      const response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${test.coords.latitude}&lon=${test.coords.longitude}&appid=${weatherAPIKey}`);
      const data = response.data.name;

      dispatch(fetchCurrentGeoSuccess(data));
    } catch (e) {
      dispatch(fetchCurrentGeoFailure(e));
    }
  }
}

export function fetchWeatherRequest() {
  return {
    type: FETCH_WEATHER_REQUEST
  }
}

export function fetchWeatherSuccess(data) {
  return {
    type: FETCH_WEATHER_SUCCESS,
    data
  }
}

export function fetchWeatherFailure(e) {
  return {
    type: FETCH_WEATHER_FAILURE,
    error: e
  }
}

export function fetchCurrentGeoRequest() {
  return {
    type: FETCH_CURRENT_GEO_REQUEST
  }
}

export function fetchCurrentGeoSuccess(currentGeo) {
  return {
    type: FETCH_CURRENT_GEO_SUCCESS,
    currentGeo
  }
}

export function fetchCurrentGeoFailure(e) {
  return {
    type: FETCH_CURRENT_GEO_FAILURE,
    error: e
  }
}
