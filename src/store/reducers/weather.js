import {
  FETCH_WEATHER_REQUEST,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_FAILURE, FETCH_CURRENT_GEO_REQUEST, FETCH_CURRENT_GEO_SUCCESS, FETCH_CURRENT_GEO_FAILURE
} from "../constants/weather";

const initialState = {
  data: {},
  currentGeo: 'Unknown geo location',
  title: 'Weather',
  error: '',
  isFetching: false,
  isFetchingGeo: false
};

export function weatherReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_WEATHER_REQUEST:
      return {...state, isFetching: true, error: ''};
    case FETCH_WEATHER_SUCCESS:
      return {...state, isFetching: false, error: '', data: action.data};
    case FETCH_WEATHER_FAILURE:
      return {...state, isFetching: false, error: action.error, data: []};

    case FETCH_CURRENT_GEO_REQUEST:
      return {...state, isFetchingGeo: true, error: ''};
    case FETCH_CURRENT_GEO_SUCCESS:
      return {...state, isFetchingGeo: false, error: '', currentGeo: action.currentGeo};
    case FETCH_CURRENT_GEO_FAILURE:
      return {...state, isFetchingGeo: false, error: action.error, currentGeo: ''};

    default:
      return state;
  }
}